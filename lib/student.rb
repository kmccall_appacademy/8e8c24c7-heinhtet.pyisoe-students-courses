require 'course'

class Student
  attr_reader :name
  attr_reader :courses
  attr_accessor :course_load

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
    @course_load = Hash.new(0)
  end

  def first_name
     @first_name
  end

  def last_name
    @last_name
  end

  def name
    @first_name + " "+ @last_name
  end

  def courses
    @courses
  end

  def course_load
    @courses.each do |el|
      @course_load[el.department] += el.credits
    end
    @course_load
  end

  def enroll(new_course)
    raise "Scheduling conflict detected" if self.has_conflict?(new_course)
    courses << new_course unless courses.include?(new_course)
    new_course.students << self unless new_course.students.include?(self)
  end

  def has_conflict?(new_course)
    self.courses.each do |course|
      return true if new_course.conflicts_with?(course)
    end
    false
  end

end
