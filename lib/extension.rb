require 'student'
require 'course'
extend 'course'
class Course
  attr_reader :name
  attr_reader :department
  attr_reader :credits
  attr_reader :days
  attr_reader :timeblock
  attr_accessor :students

  def days
    @days
  end

  def time_block
    @timeblock
  end

  def conflicts_with?(othercourse)
    days.each do |day|
      if day.include?(othercourse.days)
        return true if timeblock == othercourse.timeblock
      end
    end
    false
  end

end
